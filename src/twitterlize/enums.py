class DataSource(object):
    TWITTER = 1
    FACEBOOK = 2
    WIKIPEDIA = 3
    GOOGLEPLUS = 4

class EntityType(object):
    TwitterHashtag = "ht"
    TwitterUserMention = "um"

